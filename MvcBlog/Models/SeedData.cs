﻿using Microsoft.EntityFrameworkCore;
using MvcBlog.Data;

namespace MvcBlog.Models
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MvcBlogContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<MvcBlogContext>>()))
            {
                // Look for any movies.
                if (context.Blog.Any())
                {
                    return;   // DB has been seeded
                }

                context.Blog.AddRange(
                    new Blog
                    {
                        Title = "When Harry Met Sally",
                        Des = "dawdawdaw",
                        Detail = "dawdawdawd",
                        Category = 1,
                        Public = "adw",
                        Data_pubblic = DateTime.Now,
                        Thumbs = "dawdawd"
                    }

                );
                context.SaveChanges();
            }
        }
    }
}
