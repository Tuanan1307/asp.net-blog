﻿using System.ComponentModel.DataAnnotations;

namespace MvcBlog.Models
{
    public class Blog
    {
        public int Id { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 3)]
        public string Title { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 3)]
        public string Des { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 3)]
        public string Detail { get; set; }

        [Required]
        [Range(1, 10)]
        public int Category { get; set; }

        public string Public { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Data_pubblic { get; set; }

        [Required]
        public int Position { get; set; }

        public string Thumbs { get; set; }
    }
}
