﻿using Microsoft.EntityFrameworkCore;

namespace MvcBlog.Data
{
    public class MvcBlogContext : DbContext
    {
        public MvcBlogContext(DbContextOptions<MvcBlogContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<MvcBlog.Models.Blog> Blog { get; set; } = default!;
    }
}
